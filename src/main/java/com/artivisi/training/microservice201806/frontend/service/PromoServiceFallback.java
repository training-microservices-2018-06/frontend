package com.artivisi.training.microservice201806.frontend.service;

import com.artivisi.training.microservice201806.frontend.dto.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
public class PromoServiceFallback implements PromoService {
    @Override
    public Iterable<Product> dataSemuaProduk() {
        return new ArrayList<>();
    }

    @Override
    public Map<String, Object> backendInfo() {
        Map<String, Object> hasilFallback = new HashMap<>();
        hasilFallback.put("host", "localhost");
        hasilFallback.put("ip", "127.0.0.1");
        hasilFallback.put("port", -1);
        return hasilFallback;
    }
}
