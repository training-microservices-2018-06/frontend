package com.artivisi.training.microservice201806.frontend.service;

import com.artivisi.training.microservice201806.frontend.dto.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(name="promo", fallback = PromoServiceFallback.class)
public interface PromoService {

    @GetMapping("/product/")
    Iterable<Product> dataSemuaProduk();

    @GetMapping("/hostinfo")
    public Map<String, Object> backendInfo();
}
